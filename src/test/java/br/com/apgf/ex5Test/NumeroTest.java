
package br.com.apgf.ex5Test;

import br.com.apgf.ex5.Numero;
import br.com.apgf.ex5.OperadorPI;
import br.com.apgf.ex5.OperadorPN;
import org.junit.Test;
import static org.junit.Assert.*;

public class NumeroTest {
    @Test
    public void deveMostrarNumeroPositivo(){
    Numero numero = new Numero(1);
    OperadorPN calculo = new OperadorPN();
    String resultado = calculo.calculoPN(numero);
    assertEquals(calculo.POSITIVO, resultado);
    }
    
    @Test
    public void deveMostrarNumeroNegativo(){
    Numero numero = new Numero(-1);
    OperadorPN calculo = new OperadorPN();
    String resultado = calculo.calculoPN(numero);
    assertEquals(calculo.NEGATIVO, resultado);
    }
    
    @Test
    public void deveMostrarNumeroImpar(){
    Numero numero = new Numero(1);
    OperadorPI calculo2 = new OperadorPI();
    double resultado = calculo2.calculoPI(numero);
    assertEquals(calculo2.IMPAR, resultado, 0.001);
    }
     
    @Test
    public void deveMostrarNumeroPar(){
    Numero numero = new Numero(2);
    OperadorPI calculo2 = new OperadorPI();
    double resultado = calculo2.calculoPI(numero);
    assertEquals(calculo2.PAR, resultado, 0.001);
    }
}
