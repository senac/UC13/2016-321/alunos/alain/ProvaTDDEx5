
package br.com.apgf.ex5;

public class OperadorPN {
  
    public static final String POSITIVO = "Positivo";
    public static final String NEGATIVO = "Negativo";
        
    public String calculoPN(Numero numero){
        if (numero.getNumero() >= 0){
            return POSITIVO;
        }
            return NEGATIVO;
    }
                   
}        

