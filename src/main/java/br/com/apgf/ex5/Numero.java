
package br.com.apgf.ex5;

public class Numero {
  
    private double numero;

    public Numero(double numero) {
        this.numero = numero;
    }
    
    public double getNumero() {
        return numero;
    }   

    public double DivPAR(){
        return numero % 2; 
        
    }    
    
}
