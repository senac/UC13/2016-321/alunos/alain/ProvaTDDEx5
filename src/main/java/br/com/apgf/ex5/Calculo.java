package br.com.apgf.ex5;

import java.util.Scanner;

public class Calculo {
 
public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int valor;

        System.out.print("Digite um número inteiro: ");
        valor = entrada.nextInt();

        if (valor % 2 == 0) {
            System.out.print("O número informado é par.\n");
        } else {
            System.out.print("O número informado é Impar.\n");
        }
        if (valor > 0) {
            System.out.print("O número informado é positivo.\n");
        } else {
            System.out.print("O número informado é negativo.\n");
        }

    }

}
